//*******************************************************
// Vector2D.java
// created by Sawada Tatsuki on 2018/03/09.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ベクトルのオブジェクト */

import java.util.*;

public class Vector2D {
    protected double x;
    protected double y;

    //コンストラクタ(デフォルト(0,0))
    public Vector2D(){
		setLocation(0, 0);
    }

    //コンストラクタ(座標を設定)
    public Vector2D(double x, double y){
		setLocation(x, y);
    }

    //座標を設定
    public void setLocation(double x, double y){
		this.x = x;
		this.y = y;
    }

    //座標を設定
    public void setLocation(Vector2D p){
		this.x = p.getX();
		this.y = p.getY();
    }

    //xを設定
    public void setX(double x) {
		this.x = x;
    }
    
    //yを設定
    public void setY(double y) {
		this.y = y;
    }

    //xを取得
    public double getX(){
		return this.x;
    }
    
    //yを取得    
    public double getY(){
		return this.y;
    }

    //thisと引数の和を返す
    public Vector2D add(double x, double y) {
		return new Vector2D(this.getX() + x, this.getY() + y);
    }

    //thisと引数の和を返す
    public Vector2D add(Vector2D p) {
		return new Vector2D(this.getX() + p.getX(), this.getY() + p.getY());
    }
    
    //thisと引数の差を返す
    public Vector2D subtract(Vector2D p) {
		return new Vector2D(this.getX() - p.getX(), this.getY() - p.getY());
    }
    
    //2引数の二乗距離を返す
    public static double distanceSq(Vector2D a, Vector2D b) {
		double X = (a.getX() - b.getX()); //距離のx成分
		double Y = (a.getY() - b.getY()); //距離のy成分
		return X * X + Y * Y;
    }
    
    //2引数の距離を返す
    public static double distance(Vector2D a, Vector2D b) {
		return Math.sqrt(distanceSq(a, b)); //二乗距離のルートを返す
    }

    //引数の長さを返す
    public static double norm(Vector2D a) {
		Vector2D zero = new Vector2D(0, 0);
		return distance(zero, a);
    }
    
    //2引数の内積を返す
    public static double inner(Vector2D a, Vector2D b) {
		return a.getX() * b.getX() + a.getY() * b.getY();
    }

    //なす角の余弦を返す
    public static double cos(Vector2D a, Vector2D b) {
		return inner(a, b) / (norm(a) * norm(b));
    }
    
    //なす角を返す
    public static double angle(Vector2D a, Vector2D b) {
		return Math.acos(cos(a, b));
    }


    //ベクトルをスカラー倍する
    public Vector2D scalarMultiply(double k) {
		return new Vector2D(k * this.getX(), k * this.getY());
    }
    
    //ベクトルをスカラー倍する
    public static Vector2D scalarMultiply(Vector2D a, double k) {
		return new Vector2D(k * a.getX(), k * a.getY());
    }    
    
    //thisを正規化する
    public Vector2D normalize(){
		Vector2D x = new Vector2D(0, 0); //返すベクトル
		double norm = norm(this); //自分の長さ
		if(norm != 0) {
			x = scalarMultiply(1.0 / norm);
		}
		return x;
    }

    //引数を正規化する
    public static Vector2D normalize(Vector2D a){
		Vector2D x = new Vector2D(0, 0); //返すベクトル
		double norm = norm(a); //aの長さ
		if(norm != 0) {
			x = scalarMultiply(a, 1.0 / norm);
		}
		return x;
    }
    
    //thisのコピーを返す
    public Vector2D clone(){
		return new Vector2D(this.x, this.y);
    }
}
