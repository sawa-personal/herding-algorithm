//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2018/03/09.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingにおける描画処理を行うメインクラス． */

import java.util.*;
import processing.core.*;

public class Main extends PApplet {
    public final int WIDTH = 600 + 150; //ウィンドウの横幅
    public final int HEIGHT = 600 + 150; //ウィンドウの縦幅
    public final int DIAMETER = 8; //粒子の直径
    private PApple[] sheep; //羊のアイコン
    private PApple[] shepherd; //牧羊犬のアイコン
    private PSquare target; //目標値のアイコン
    private PSquare gcm; //羊の重心のアイコン
    private int num = Simulator.num; //羊の数
    private int shepherdNum = Simulator.shepherdNum; //牧羊犬の数
    private Observer observer; //オブザーバ
    
    public void settings() {
		size(WIDTH, HEIGHT); //ウィンドウサイズを指定
    }

    public void setup() {	
		observer = new Observer(this); //オブザーバ
		strokeWeight(0.24f); //枠線の太さ
		target = observer.getTarget(); //ターゲットのアイコンを取得
		PColor targetColor = new PColor(75, 167, 206); //青色
		target.setColor(targetColor);
    }
    
    public void draw() {
		scale(4f, 4f); //4倍に拡大
		//frameRate(5);
		observer.run(); //系を動かす
		background(255); //背景色を指定．画面をリセットする役割もある．
	
		//ターゲットを描画
		target.draw();

		//Pd or Pcを描画
	
		//羊を描画
		sheep = observer.getPosition(); //羊のアイコンを取得
		PColor sheepColor = new PColor(144, 177, 226); //白色
		for(int i = 0; i < num; i++){
			sheep[i].setColor(sheepColor);
			sheep[i].draw();
		}
	
		//GCMを描画
		gcm = observer.getGCM();
		PColor gcmColor = new PColor(240, 32, 32); //赤色
		gcm.setColor(gcmColor);
		gcm.draw();

		//牧羊犬を描画
		shepherd = observer.getShepherd(); //牧羊犬のアイコンを取得
		PColor shepherdColor = new PColor(255, 192, 0); //オレンジ色
		for(int i = 0; i < shepherdNum; i++){
			shepherd[i].setColor(shepherdColor);
			shepherd[i].draw();
		}

		//fを描画
		Vector2D gcmPosition = gcm.position;
		fill(255, 16);
		ellipse((float)gcmPosition.getX(), (float)gcmPosition.getY(), 2 * (float)Simulator.f, 2 * (float)Simulator.f);	
    }
    
    public static void main(String args[]) {
		PApplet.main("Main"); //Mainクラスを呼び出してProcessingを起動
    }
}
