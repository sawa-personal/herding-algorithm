//*******************************************************
// Agent.java
// created by Sawada Tatsuki on 2018/03/08.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントのオブジェクト */

import java.util.*;

public abstract class Agent {
    public Vector2D p; //座標
    public Vector2D v; //速度
    public double range; //認識範囲
    public double angle; //視野角
    
    public int id; //エージェントのID
    public Simulator simulator; //属するネットワーク
    
    private Random rnd = new Random(); //乱数生成用インスタンス
        
    //コンストラクタ
    public Agent(int id, Simulator simulator) {
		p = new Vector2D(); //座標
		v = new Vector2D(); //速度
		range = -1; //認識範囲
		angle = 360; //視野角
	
		this.id = id; //IDを設定
		this.simulator = simulator; //属するシミュレータを設定
    }

    //座標を設定
    public void setPosition(double x, double y) {
		this.p.setLocation(x, y);
    }

    //速度を設定
    public void setVelocity(double x, double y) {
		this.v.setLocation(x, y);	
    }

    //認識範囲を設定
    public void setRange(double range) {
		this.range = range;
    }

    //視野角を設定
    public void setAngle(double angle) {
		if(range < 0 || 360 < range) {
			System.exit(1); //0<=θ<=360範囲外で強制終了
		}
		this.angle = angle;
    }
	
	/*
    //指定のエージェントが視野内にいるか
    public boolean sight(AbstractAgent op) {
		boolean b = false;
		Vector pq = op.p.subtract(p);	//自分から相手に向かうベクトル
		double cos = Vector.inner(v, pq) / Vector.norm(v) / Vector.norm(pq); //余弦
		
		//補正
		if(cos > 1) {
			cos = 1.0;
		}
		if(cos < -1) {
			cos = -1.0;
		}		
		
		if(minCos <= cos) {
			b = true;
		}
		return b;
    }
	*/	
}
