//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/03/08.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータ本体のクラス． */

import java.util.*;

public class Simulator {
    public static int num = 200; //羊の数
    public static int shepherdNum = 1; //牧羊犬の数

    public static double f = 3 * Math.log(num); // 2/3乗する．すべての羊がGCMを中心としてf以内に居れば，牧羊犬はdrive()する．羊が1匹でもfの外に居るときには，牧羊犬はcollect()に移る．
    
    public Sheep[] sheep; //羊エージェント
    public Shepherd[] shepherd; //牧羊犬エージェント

    public Vector2D GCM = new Vector2D(0, 0); //羊たちの大域的な重心

    public static final int L = 150; //平方空間の1辺の長さ
    
    private static Random rnd = new Random(); //乱数生成用インスタンス

    //コンストラクタ
    public Simulator() {
		sheep = new Sheep[num]; //羊インスタンスの生成
		for(int i = 0; i < num; i++) {
			sheep[i] = new Sheep(i, this); //羊インスタンス生成
			sheep[i].setPosition(130 + 20 * rnd.nextDouble(), 130 + 20 * rnd.nextDouble()); //右下4分の1のエリアに配置
			double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
			//sheep[i].setVelocity(Math.random() * Math.cos(r), Math.random() * Math.sin(r)); //大きさ一定，方向ランダムな速度
		}

		shepherd = new Shepherd[shepherdNum]; //牧羊犬インスタンスの生成
		for(int i = 0; i < shepherdNum; i++) {
			shepherd[i] = new Shepherd(i, this); //牧羊犬インスタンス生成
			shepherd[i].setPosition(rnd.nextInt(L / 2), L / 2 + rnd.nextInt(L / 2)); //左下4分の1に配置
			double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
			shepherd[i].setVelocity(Math.random() * Math.cos(r), Math.random() * Math.sin(r)); //大きさ一定，方向ランダムな速度
		}
    }
    
    public void run(){
		//羊を動かす
		for(int i = 0; i < num; i++) {
			sheep[i].move();
		}
	
		//羊たちの重心(GCM)を計算
		GCM.setLocation(0, 0); //重心を初期化
		for(int i = 0; i < num; i++) {
			GCM = GCM.add(sheep[i].p);
		}
		GCM = GCM.scalarMultiply(1.0 / num); //重心を算出
	
		int furhest = 0; //群れから最も離れた羊の添字
		double dFurthest = 0; //群れの重心と，最も離れた羊の間の距離の最大値
	
		//重心から，最も離れている羊までの距離とその添字を求める
		for(int i = 0; i < num; i++) {
			double tmp = Vector2D.distance(GCM, sheep[i].p);
			if(dFurthest < tmp) {
				dFurthest = tmp;
				furhest = i;
			}
		}
	
		//牧羊犬を動かす
		for(int i = 0; i < shepherdNum; i++) {
			shepherd[i].move(f, dFurthest, sheep[furhest]);
		}
    }

    public static void main(String args[]) {
		Simulator simulator = new Simulator(); //シミュレータを生成
		int iteration = 1000; //試行回数
		for(int i = 0; i < iteration; i++) {
			simulator.run(); //シミュレータを起動
		}
    }
}
