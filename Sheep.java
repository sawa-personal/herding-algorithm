//*******************************************************
// Sheep.java
// created by Sawada Tatsuki on 2018/03/08.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 羊のオブジェクト */

public class Sheep extends Agent {
	public static double delta = 1.0; //羊の最大速度
	
	private Vector2D Rs; //牧羊犬から受けるベクトル
	public static double rs = 65; //牧羊犬から逃げようとする範囲
	public static double ra = 2; //羊同士が反発する範囲

	private double prob = 0.05; //放牧中，単位ステップごとに移動する確率
	
	public static double rhoa = 2; //(1番目に大きい値) 他の羊から受けるベクトルの大きさ
	public static double c = 1.05; //(2番目に大きい値) LCMへ向かうベクトルの大きさ
	public static double rhos = 1.00; //(3番目に大きい値) 牧羊犬から受けるベクトルの大きさ
	public static double h = 0.5; //(4番目に大きい値) 慣性ベクトルの大きさ
	public static double e = 0.3; //誤差項の強度

	//コンストラクタ
	public Sheep(int id, Simulator simulator) {
		super(id, simulator); //コンストラクタを継承
	}
	
	//羊は2つのルールrun()とgraze()に従って動く?
	public void move() {
		//近傍に牧羊犬がいるかどうか
		boolean shepherdIsNear = false;
		for(int i = 0; i < simulator.shepherdNum; i++) {
			double distance = Vector2D.distance(this.p, simulator.shepherd[i].p); //自分と牧羊犬との距離
			if(distance < rs) { //自分との距離がrs内に牧羊犬がいる牧羊犬に対して
				shepherdIsNear = true;
			}
		}
		Vector2D F = new Vector2D(0, 0); //羊が受ける総合ベクトル
	
		/********** start: 近隣の羊から受けるベクトルを計算 **********/
		Vector2D Ra = new Vector2D(0, 0); //近隣の羊から受けるベクトル
		for(int i = 0; i < simulator.num; i++) {
			double distance; //自分と近傍羊の距離
			if(id == i) {
				continue; //自分はスキップ
			}		
			distance = Vector2D.distance(this.p, simulator.sheep[i].p); //自分と近傍の距離を算出		
			if(id != i && distance < ra) { //自分以外で，自分との距離がra内にいる者に対して
				Vector2D u = this.p.subtract(simulator.sheep[i].p); //近傍から自分へ向かうベクトル
				u = u.normalize(); //正規化
				Ra = Ra.add(u); //Raに加算
			}
		}
		Ra = Ra.normalize(); //Raを正規化
		Ra = Ra.scalarMultiply(rhoa); //Raの大きさをρaに
		F = F.add(Ra); //総合ベクトルに加算
		/********** end: 近隣の羊から受けるベクトルを計算 **********/
	
		/********** start: (2)LCMへ向かうベクトルを計算 **********/
		if(shepherdIsNear) {
			//羊たちの重心(LCM)を計算
			Vector2D LCM = new Vector2D(0, 0); //近傍の羊の局所的な重心
			double nCount = 0; //近傍数のカウント用
			for(int i = 0; i < simulator.num; i++) {
				if(id == i) {
					continue; //自分はスキップ
				}
				double distance = Vector2D.distance(this.p, simulator.sheep[i].p); //自分と近傍羊との距離
				if(distance < rs) {
					LCM = LCM.add(simulator.sheep[i].p);
					nCount++; //近傍数をカウント
				}
			}
		
			if(nCount != 0) {
				LCM = LCM.scalarMultiply(1.0 / nCount); //重心を算出
			}
		
			Vector2D C = LCM.subtract(this.p); //自分からLCMへ向かうベクトル
			C = C.normalize(); //Cを正規化
			C = C.scalarMultiply(c); //Cの大きさをcに
			F = F.add(C); //総合ベクトルに加算
		}
		/********** end: (2)LCMへ向かうベクトルを計算 **********/

		/********** start: (3)牧羊犬から受けるベクトルを計算 **********/
		if(shepherdIsNear) {
			Vector2D Rs = new Vector2D(0, 0);
		
			for(int i = 0; i < simulator.shepherdNum; i++) {
				double distance; //自分と牧羊犬の距離
				distance = Vector2D.distance(this.p, simulator.shepherd[i].p); //自分と近傍の距離を算出		
				if(distance < rs) { //自分との距離がrs内にいる牧羊犬に対して
					Vector2D u = this.p.subtract(simulator.shepherd[i].p); //牧羊犬から自分へ向かうベクトル
					u = u.normalize(); //正規化
					Rs = Rs.add(u); //Rsに加算
				}
			}
			Rs = Rs.normalize(); //Rsを正規化
			Rs = Rs.scalarMultiply(rhos); //Rsの大きさをρsに
			F = F.add(Rs); //総合ベクトルに加算
		}
		/********** end: (3)牧羊犬から受けるベクトルを計算 **********/
	
		/********** start: (4)慣性項を計算 **********/
		if(shepherdIsNear) {
			v.setLocation(0, 0);
			Vector2D H = v.normalize(); //慣性項．現在の速度を正規化
			H = v.scalarMultiply(h); //Hの大きさをhにする
			F = F.add(H); //総合ベクトルに加算
		}
		/********** end: (4)慣性項を計算 **********/

		/********** start: (5)ノイズ項を計算 **********/
		double rndRad = Math.random() * 2.0 * Math.PI; //ランダムな角度
		Vector2D E = new Vector2D(Math.random() * Math.cos(rndRad), Math.random() * Math.sin(rndRad)); //ランダムな方向ベクトル
		E = E.normalize(); //Eを正規化
		E = E.scalarMultiply(e); //Eの大きさをeに
		F = F.add(E); //総合ベクトルに加算
		/********** end: (5)ノイズ項を計算 **********/

		F = F.normalize(); //Fを正規化
		F = F.scalarMultiply(delta); //Fの大きさを最大速さに
		v = F; //速度を更新
		p = p.add(v); //位置を更新
		
		/*
		  else { //牧羊犬が近くにいなかったとき
		  if(Math.random() < prob) {
		  double rndRad = Math.random() * 2.0 * Math.PI; //ランダムな角度
		  Vector2D E = new Vector2D(Math.random() * Math.cos(rndRad), Math.random() * Math.sin(rndRad)); //ランダムな方向ベクトル
		  E = E.normalize(); //Eを正規化
		  E = E.scalarMultiply(delta); //Eの大きさをdeltaに
		  v = E;
		  p = p.add(v);
		  }
		*/
	}
}
