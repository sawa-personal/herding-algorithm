//*******************************************************
// Shepherd.java
// created by Sawada Tatsuki on 2018/03/08.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 牧羊犬のオブジェクト */

public class Shepherd extends Agent {
    public static double delta = 1.5; //牧羊犬の最大速度
    public static double e = 0.3; //誤差項の強度
    
    public Vector2D Pd = new Vector2D(); //drive時の牧羊犬が目指す場所．直線gcm→target上の，gcm(=羊たちの重心)よりra√numだけ後ろ．
    public Vector2D Pc = new Vector2D(); //collect時の牧羊犬が目指す場所．直線furtherstSheep→gcm上の，furthestSheep(=最も離れている羊)よりra*mだけ後ろ．
    public Vector2D target = new Vector2D(10, 10); //目的地の座標
    public double b = Math.PI / 2; //背後の死角
    public double m = 2; //Pcの位置，furtherSheepから羊m個分下がる

    //コンストラクタ
    public Shepherd(int id, Simulator simulator) {
		super(id, simulator); //コンストラクタを継承
    }
    
    //最も離れた羊がfより離れていたとき牧羊犬は収集し，そうでなければ駆動する
    public void move (double f, double dFurthest, Sheep furhestSheep) {
		if(dFurthest > f) { //ルール1)収集:バラバラになった羊を集める
			Vector2D dir = simulator.GCM.subtract(furhestSheep.p); //furhestSheepからGCMまでの方向ベクトル
			dir = dir.normalize(); //方向ベクトルを正規化
			dir = dir.scalarMultiply(-m * Sheep.ra); //方向ベクトルの大きさをm*raに
			Pc = furhestSheep.p.add(dir); //furthestSheepからdirだけ下がる位置がPc
			v = Pc.subtract(p); //速度を更新
		} else { //ルール2)駆動:密集した羊を前に押し出す
			//driveで進まない問題/
			Vector2D dir = target.subtract(simulator.GCM); //GCMからtargetまでの方向ベクトル
			dir = dir.normalize(); //方向ベクトルを正規化
			dir = dir.scalarMultiply(- Sheep.ra * Math.sqrt(simulator.num)); //方向ベクトルの大きさを-ra√numに
			Pd = simulator.GCM.add(dir); //GCMからdirだけ下がる位置がPd
			v = Pd.subtract(p); //速度を更新
		}
		v = v.normalize(); //速度を正規化
		v = v.scalarMultiply(delta); //最高速度に調整
		p = p.add(v); //位置を更新	
    }
}
