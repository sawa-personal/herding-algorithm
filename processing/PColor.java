//*******************************************************
// PColor.java
// created by Sawada Tatsuki on 2018/03/10.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 色のオブジェクト */

public class PColor {
    public int r; //赤色
    public int g; //緑色
    public int b; //青色
    public int a; //透明度

    //コンストラクタ
    public PColor(int... c) {
		switch(c.length) {
		case 1: setColor(c[0]); break;
		case 2: setColor(c[0], c[1]); break;
		case 3: setColor(c[0], c[1], c[2]); break;
		case 4: setColor(c[0], c[1], c[2], c[4]); break;
		default: System.exit(1);
		}
    }

    //1値指定のときグレイスケール
    public void setColor(int v) {
		this.r = color(v);
		this.g = color(v);
		this.b = color(v);
		this.a = color(255);
    }

    //2値指定のときグレイスケール + 透明度
    public void setColor(int v, int a) {
		this.r = color(v);
		this.g = color(v);
		this.b = color(v);
		this.a = color(a);
    }

    //3値指定のときカラー
    public void setColor(int r, int g, int b) {
		this.r = color(r);
		this.g = color(g);
		this.b = color(b);
		this.a = color(255);
    }
    
    //4値指定のときカラー + 透明度
    public void setColor(int r, int g, int b, int a) {
		this.r = color(r);
		this.g = color(g);
		this.b = color(b);
		this.a = color(a);
    }

    //0<=c<=255の範囲外指定で強制終了
    private int color(int c) {
		if(c < 0 || 255 < c) {
			System.exit(1);
		}
		return c;
    }
}
