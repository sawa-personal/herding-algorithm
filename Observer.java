//*******************************************************
// Observer.java
// created by Sawada Tatsuki on 2018/03/09.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータから座標情報などを観測し，Processing描画用のデータに変換するクラス */

import java.util.*;
import processing.core.*;

public class Observer {
    public final int num = Simulator.num; //粒子の総数
    public final int shepherdNum = Simulator.shepherdNum; //粒子の総数
    private Simulator simulator; //シミュレータのインスタンス
    private PApplet pa; //呼び出し元のPApplet

    //コンストラクタ
    public Observer(PApplet pa) {
		this.pa = pa; //呼び出し元のPApplet
		this.simulator = new Simulator(); //シミュレータを生成
    }  

    //系を動かす
    public void run() {
		simulator.run();
    }
    
    //羊のアイコンを取得
    public PApple[] getPosition() {
		PApple[] sheep = new PApple[num];
		for(int i = 0; i < num; i++) {
			sheep[i] = new PApple(pa,
								  simulator.sheep[i].p.clone(),
								  simulator.sheep[i].v.clone(),
								  Sheep.ra / 1.8);
		}
		return sheep;
    }

    //牧羊犬のアイコンを取得
    public PApple[] getShepherd() {
		PApple[] shepherd = new PApple[shepherdNum];
		for(int i = 0; i < shepherdNum; i++) {
			shepherd[i] = new PApple(pa,
									 simulator.shepherd[i].p.clone(),
									 simulator.shepherd[i].v.clone(),
									 Sheep.ra / 1.8);
		}
		return shepherd;
    }

    public PSquare getTarget() {
		PSquare target = new PSquare(pa, simulator.shepherd[0].target, Sheep.ra);
		return target;
    }
    
    public PSquare getGCM() {
		PSquare GCM = new PSquare(pa, simulator.GCM, Sheep.ra);
		return GCM;
    }
    /*
	  public PSquare getP() {
	  PSquare P = new PSquare();
	  return P;
	  }
    */
}
